package nl.utwente.mod4.pokemon;

import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerRequestFilter;
import jakarta.ws.rs.ext.Provider;

@Provider
public class RequestFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) {
        System.out.println("Incoming Request:");
        System.out.println("Method: " + requestContext.getMethod());
        System.out.println("URI: " + requestContext.getUriInfo().getRequestUri());
        System.out.println("Headers: " + requestContext.getHeaders());
    }

}
